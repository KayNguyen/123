
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form method="POST" action="form-handler" onsubmit="return checkForm(this);">

    ...

    <p><img src="/captcha.php" width="120" height="30" border="1" alt="CAPTCHA"></p>
    <p><input type="text" size="6" maxlength="5" name="captcha" value=""><br>
        <small>copy the digits from the image into this box</small></p>

    ...

</form>
</body>
</html>