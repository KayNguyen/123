<?php
session_start();
$cap = 'notEq';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($_POST['captcha'] == $_SESSION['cap_code']) {
        // Captcha verification is Correct. Do something here!
        $cap = 'Eq';
    } else {
        // Captcha verification is wrong. Take other action
        $cap = '';
    }
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Captcha - PHP | Jquery</title>
<style>
h1 {  font-family: Helvetica, Arial, sans-serif;  text-align: center; font-size:50px; margin-top:20px; color:#fff;
	  text-shadow: 2px 2px 0px rgba(255,255,255,.7), 5px 7px 0px rgba(0, 0, 0, 0.1); 
}
body{background:url(../background1.png); font-family:Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif}
label{font-size:20px; color:#fff}
#name,#msg,#captcha{
    width: 60%;
    height: 32px;
    box-sizing: border-box;
    border-radius: 5px;
    border: 1px solid #ccc;
    font-size: 18px;
    font-family: Montserrat;
    outline: none;
	padding:6px;
}
#submit {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 26px;
    border: none;
    cursor: pointer;
    width: 90%;
	font-size:20px;
}
#submit:hover {
    opacity: 0.8;
}
.cap_status
{
width: 350px;
padding: 10px;
font: 14px arial;
color: #fff;
background-color: #10853f;
display: none;
}
.cap_status_error
{
background-color: #bd0808;
}
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#submit').click(function(){
		var name = $('#name').val();
		var msg = $('#msg').val();
        var captcha = $('#captcha').val();                    

        if(name.length != 0 && msg.length != 0 && captcha.length != 0){
               return true;
            }
             return false;
     });

  var capch = '<?php echo $cap; ?>';
  if(capch != 'notEq'){
     if(capch == 'Eq'){
         $('.cap_status').html("Your form is successfully Submitted ").fadeIn('slow').delay(3000).fadeOut('slow');
         }
		 else{
               $('.cap_status').html("Human verification Wrong!").addClass('cap_status_error').fadeIn('slow');
               }
        }
});
</script>

</head>
<body>
<h1>Captcha Verification Code</h1>
<div style="width:400px; margin:auto; border:4px solid rgba(255,255,255,0.6); padding:20px">
<!---All fields are mandatory--->
<form action="" method="post">
 <div style="width:100%; margin-bottom:20px">
   <label>Name:</label><br/>
   <input type="text" name="name" id="name"/>
 </div>
 
 <div style="width:100%; margin-bottom:20px">
   <label>Message:</label><br/>
   <textarea  name="msg" id="msg" style="height:100px; width:100%"></textarea>
 </div>
       
 <div style="width:100%; text-align:center">
 <img src="captcha.php" style="width:150px; height:30px"/><a href="index.php" style="color:#fff;"><p style="margin:4px 0px 0px 0px">Reload Image</p></a>
 </div>;
                      
 <div style="width:100%">
   <label>Enter Captcha:</label>
   <input type="text" name="captcha" id="captcha" maxlength="6" size="6"/>
 </div>
  
  <input type="submit" value="Submit" id="submit"/>
</form>
    <div class="cap_status"></div>
</div>
</body>
</html>
