<?php

/* Set current, prev and next page */
$page = (!isset($_GET['page']))? 1 : $_GET['page'];
$prev = ($page - 1);
$next = ($page + 1);

/* Số lượng bản ghi trên 1 trang*/
$max_results = 5;

/* Calculate the offset */
$from = (($page * $max_results) - $max_results);

/* Đọc dữ liệu từ file data.json */
$result = file_get_contents('data.json');
$result = json_decode($result);
$result = array_slice($result, 0, 50);
$total_results = count($result);
$total_pages = ceil($total_results / $max_results);
$pagination = '';

/* Create a PREV link if there is one */
if($page > 1)
{
    $pagination .= '<li class="page-item"><a class="page-link" href="?page='.$prev.'">Previous</a></li>';
}
/* Loop through the total pages */
for($i = max(1, $page - 4); $i <= min($page + 5, $total_pages); $i++)
{
    if(($page) == $i)
    {
        $pagination .= '<li class="page-item disabled"><a class="page-link" href="index.php?page='.$i.'">'.$i.'</a></li>';
    }
    else
    {
        $pagination .= '<li class="page-item"><a class="page-link" href="index.php?page='.$i.'">'.$i.'</a></li>';
    }
}

/* Print NEXT link if there is one */
if($page < $total_pages)
{
    $pagination .= '<li class="page-item"><a class="page-link" href="?page='.$next.'"> Next</a></li>';
}

/* Now we have our pagination links in a variable($pagination) ready to
   print to the page. I pu it in a variable because you may want to
   show them at the top and bottom of the page */

/* Below is how you query the db for ONLY the results for the current page */
$result=array_slice($result, $from, $max_results);

foreach ($result as $i)
{
    $i->name.'<br />';
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css">
    <title>Document</title>
</head>
<body>
<nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
        <?= $pagination ?>
    </ul>
</nav>
</body>
</html>