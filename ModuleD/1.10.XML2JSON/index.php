<?php
$_xml = '<note>
            <to>Tove</to>
            <from>Jani</from>
            <heading>Remember</heading>
            <body>okdhjdhđh</body>
        </note>';
$_json = '';
if(isset($_POST['xml'])) {
    $_xml = $_POST['xml'];
    $xml = simplexml_load_string('<ok>'.$_xml.'</ok>');
    $_json = json_encode($xml);
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>XML2JSON Converter</title>
</head>
<body>
    <div style="margin: 0 auto; background-color: #CCCCCC; width: 80%">
        <form action="" method="POST" style="padding: 10px;">
            <div style="display:flex; justify-content: space-between">
                <div style="width: 50%;">
                    <textarea name="xml" id="xml" style="width: 100%;" rows="20">
                        <?= $_xml ?>
                    </textarea>
                </div>
                <div style="width: 50%;">
                    <textarea name="json" id="json" style="width: 100%;" rows="20">
                    <?= $_json ?>
                </textarea>
                </div>
            </div>
            <div style="display: block">
                <button>Convert</button>
            </div>
        </form>
    </div>
    <script>
        var textedJson = JSON.stringify('<?= $_xml ?>');
        document.getElementById("json").value = textedJson;
    </script>
</body>
</html>
