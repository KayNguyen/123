<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'module_d');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{7yl]L&:[~SC_8J/rpenuo>{>%vn$W~jK=;81@Wq._/yYgxrK*?`s(wJ(5c Cp A');
define('SECURE_AUTH_KEY',  'UC%P:Nr(7lV>p!F(4oHx]u[gw>Ixy+9eBKYg|mF>ZglLFvqw3ZSEBn()G OaEDv$');
define('LOGGED_IN_KEY',    '[0E5S}nD~1~;q9KB+NGDvt0rUXwe[Js|&*uXF_QPP*$_H|W|)s#81hAS%sdIMmd:');
define('NONCE_KEY',        'wo9k6*TL=e~h }PSJ8k`K4EcGuY%j%<Y3MvTOgOKBLg4vvzb^>hiK^RWd=~;tl~(');
define('AUTH_SALT',        ')$Z0o=,s|R!6Xx)yUH]%umNu%[g]szh4$Y-^Y6xr2{IJSL@d3KHTkZ;hAAJ4c/`.');
define('SECURE_AUTH_SALT', '$t->sQ$d+hl~ztv.1B#L82U7Xs#tu&PHQ>vOxcVsaQ7vq*o?)}u)HNZ5{{;**nqN');
define('LOGGED_IN_SALT',   'zxhhBa%Q7fHU/+,Px&-|4Pu~}6u-o>.D<jlq8nSWXTrzUVE24(]Fd epmMikkoP(');
define('NONCE_SALT',       'lI$C@c:]iRut^:&/3Sf66Gt([[O@t@c%jUt{pkg-l,r9Qe8,X?K*c8o}0rk<vf&e');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
