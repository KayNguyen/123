<?php
// Set your timezone
date_default_timezone_set('Asia/Tokyo');

// Get prev & next month
if (isset($_GET['ym'])) {
    $ym = $_GET['ym'];
} else {
    $ym = date('Y-m');
}
setlocale(LC_TIME, 'fr_FR');

$timestamp = strtotime($ym . '-01');
if ($timestamp === false) {
    $ym = date('Y-m');
    $timestamp = strtotime($ym . '-01');
}

$prev = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)-1, 1, date('Y', $timestamp)));
$next = date('Y-m', mktime(0, 0, 0, date('m', $timestamp)+1, 1, date('Y', $timestamp)));
$day_count = date('t', $timestamp);
$today = date('Y-m-j', time());
$str = date('w', mktime(0, 0, 0, date('m', $timestamp), 1, date('Y', $timestamp)));
$weeks = array();
$week = '';
$week .= str_repeat('<div><span class="fc-date"></span></div>', $str);
for ( $day = 1; $day <= $day_count; $day++, $str++) {

    $date = $ym . '-' . $day;

    if ($today == $date) {
        $week .= '<div><span class="fc-date">' . $day;
    } else {
        $week .= '<div><span class="fc-date">' . $day;
    }
    $week .= '</span></div>';

    // End of the week OR End of the month
    if ($str % 7 == 6 || $day == $day_count) {

        if ($day == $day_count) {
            // Add empty cell
            $week .= str_repeat('<div><span class="fc-date"></span></div>', 6 - ($str % 7));
        }

        $weeks[] = '<div class="fc-row">' . $week . '</div>';

        // Prepare for new week
        $week = '';
    }

}

?>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="calendar.css">
</head>

<body>


    <div class="custom-calendar-wrap">
        <div class="custom-inner">
            <div class="custom-header clearfix">
                <nav>
                    <a href="<?= '?ym='.$prev ?>" class="custom-btn custom-prev"></a>
                    <a href="<?= '?ym='.$next ?>" class="custom-btn custom-next"></a>
                </nav>
                <h2 id="custom-month" class="custom-month"><?= date('F', strtotime($ym)) ?></h2>
                <h3 id="custom-year" class="custom-year"><?= date('Y', strtotime($ym)) ?></h3>
            </div>

            <div id="calendar" class="fc-calendar-container">
                <div class="fc-calendar fc-five-rows">
                    <div class="fc-head">
                        <div>Sun</div>
                        <div>Mon</div>
                        <div>Tue</div>
                        <div>Wed</div>
                        <div>Thu</div>
                        <div>Fri</div>
                        <div>Sat</div>
                    </div>
                    <div class="fc-body">
                        <?php
                            foreach ($weeks as $week) {
                                echo $week;
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>


</body>

</html>