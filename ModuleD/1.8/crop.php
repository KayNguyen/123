<?php
session_start();
if($_SESSION['image_type'] == 'jpeg') {
    $img = imagecreatefromjpeg($_SESSION['image']);
}elseif ($_SESSION['image_type'] == 'jpg') {
    $img = imagecreatefromjpeg($_SESSION['image']);
}elseif ($_SESSION['image_type'] == 'png') {
    $img = imagecreatefrompng($_SESSION['image']);
}elseif ($_SESSION['image_type'] == 'gif') {
    $img = imagecreatefromgif($_SESSION['image']);
}
$imgcrop = imagecrop($img, ['x' => 0, 'y' => 0, 'width' => 655, 'height' => 455]);
if ($imgcrop !== FALSE) {
    imagepng($imgcrop, 'uploads/crops/crop_'.$_SESSION['image_name']);
    $_SESSION['image'] = 'uploads/crops/crop_'.$_SESSION['image_name'];
    $_SESSION['image_name'] = 'crop_'.$_SESSION['image_name'];
    imagedestroy($imgcrop);
    echo "Image cropped successfully";
    suRedirect('index.php', 0);
}
imagedestroy($img);

function suRedirect($url, $delay = 0) {
    echo '<meta http-equiv="refresh" content="' . $delay . '; url=' . $url . '">';
}
?>