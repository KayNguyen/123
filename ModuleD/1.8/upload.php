<?php
session_start();
/*
 * 1. Tạo thư mục chứa ảnh đã upload
 * 2. Chỉ định đường dẫn tệp đã được tải lên
 * 3. Phần mở rộng của tệp
 * 4. Kiểm tra là hình ảnh thật hay giả
 * */

$targetDir = 'uploads/';
$targetFile = $targetDir. basename($_FILES['fileToUpload']['name']);
$imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
$uploadOk = 1; // succeed = 1| failed = 0
$maximumSize = 500000;
// kiểm tra xem ảnh thật hay giả

if(isset($_POST['submit'])) {
    $check = getimagesize($_FILES['fileToUpload']['tmp_name']);
    if($check !== FALSE) {
        echo "File is an image - ".$check['mime'].".<br/>";
        $uploadOk = 1;
    }else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
    // kiểm tra xem file có tồn tại hay ko
    if(file_exists($targetFile)) {
        echo "Sorry, file already exists<br/>";
        $uploadOk = 0;
    }
    // giới hạn dung lượng upload
    if($_FILES['fileToUpload']['size'] > $maximumSize) {
        echo "Sorry, your file is too large<br/>";
        $uploadOk = 0;
    }
    // giới hạn kiểu ảnh jpg, png, jpeg, gif
    if($imageFileType != 'jpg' && $imageFileType != 'png' && $imageFileType != 'jpeg' && $imageFileType != 'gif') {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.<br/>";
        $uploadOk = 0;
    }
    // kiểm tra & thông báo upload thành công hay chưa
    if($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.<br/>";
    }else {
        if(move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $targetFile)){
            echo "The file ".basename($_FILES['fileToUpload']['name'])." uploaded.";
            $_SESSION['image'] = $targetFile;
            $_SESSION['image_name'] = $_FILES['fileToUpload']['name'];
            $_SESSION['image_type'] = $imageFileType;
            suRedirect('index.php', 0);
        }else {
            echo "The file uploading!...";
        }
    }
    echo "<a style='color: #0A246A' href='index.php'> Go home </a>";
}

function suRedirect($url, $delay = 0) {
    echo '<meta http-equiv="refresh" content="' . $delay . '; url=' . $url . '">';
}
