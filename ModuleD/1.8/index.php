<?php
session_start();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Image Cropper</title>
</head>
<body>
    <div style="width: 80%; margin: 0 auto; background-color: #fff4c3">
        <div style="padding: 8px;">
            <form action="upload.php" method="POST" enctype="multipart/form-data">
                <input name="fileToUpload" id="fileToUpload" type="file">
                <button name="submit">Submit</button>
            </form>
        </div>
    </div>
    <div style="width: 80%; margin: 0 auto;">
    <div style="padding: 8px;">
        <form action="crop.php" method="POST">
            <img id="img" src="<?= $_SESSION['image'] ?>" alt="">
            <button style="display: block; background-color: green;cursor: pointer; margin-top: 10px; color: #fff;padding: 4px 12px; border: 1px solid green" class="btn crop">Crop</button>
        </form>
    </div>

</body>
</html>
